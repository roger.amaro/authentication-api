FROM python:3.6

RUN pip install -U pipenv

WORKDIR /app

COPY Pipfile .
COPY Pipfile.lock .

RUN pipenv install


COPY . /app
EXPOSE 8000

CMD pipenv run gunicorn -w 4 wsgi:app