from flask import Flask
from src.models.user import User
from flask_restful import Api, abort
from flask_migrate import Migrate
from .config import app_config
from flask_cors import CORS
import os
from flask_jwt_extended import JWTManager
from src.models import db
import logging
from keys import PRIVATE_KEY, PUBLIC_KEY


def create_app(env_name):
    app = Flask(__name__)
    app.config.from_object(app_config[env_name])
    if app.config['SQLALCHEMY_DATABASE_URI'] is None:
        logging.error('Database URL not set!!!!!')
        exit(1)

    db.init_app(app)
    Migrate(app, db)
    app.config['JWT_ALGORITHM']="RS256"
    app.config['JWT_SECRET_KEY']=PRIVATE_KEY
    app.config['JWT_PUBLIC_KEY']=PUBLIC_KEY
    JWTManager(app)
    CORS(app)
    api = Api(app)

    from src.views.register import Registro
    from src.views.authentication import Auth
    from src.views.authentication import Refresh
    api.add_resource(Registro, "/auth/register/")
    api.add_resource(Auth, "/auth/autentication/")
    api.add_resource(Refresh, "/auth/refresh/")
    return app