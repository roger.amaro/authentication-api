import os

from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv(), override=True)

class DevelopmentConf(object):
    """
    Development environment configuration
    """
    DEBUG = True
    TESTING = False
    SQLALCHEMY_TRACK_MODIFICATIONS=False
    SQLALCHEMY_ECHO=True
    SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URL')

class ProductionConf(object):
    """
    Production environment configurations
    """
    DEBUG = False
    TESTING = False
    SQLALCHEMY_TRACK_MODIFICATIONS=False
    SQLALCHEMY_ECHO=False
    SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URL')

class TestingConf(object):
    """
    Development environment configuration
    """
    DEBUG = False
    TESTING = True
    SQLALCHEMY_TRACK_MODIFICATIONS=False
    SQLALCHEMY_ECHO=False
    SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_TEST_URL')

app_config = {
    'development': DevelopmentConf,
    'production': ProductionConf,
    'testing': TestingConf
}