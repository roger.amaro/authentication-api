import datetime
from flask import abort
from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy()
from flask_bcrypt import Bcrypt
bcrypt = Bcrypt()