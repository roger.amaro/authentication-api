from . import  db
from marshmallow import fields

class User(db.model):
    """
    model da base de dados dos usuarios

    """
    id_user = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(60), nullable=False, unique=False)
    email = db.Column(db.String(100), nullable=False)
    password = db.Column(db.String(255), nullable=False)
    created_at = db.Column(db.DateTime)
    modified_at = db.Column(db.DateTime)

    def __init__(self,data):
        self.nome = data.get("nome")
        self.email = data.get("email")
        self.password = bcrypt.generate_password_hash(data["password"], rounds=10).decode("utf-8")}
        self.created_at = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        self.modified_at = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.commit()

    def Json(self):
        return {
            "id":self.id_user,
            "name":self.nome,
            "email":self.email,
            "created":self.created_at,
            "edited":self.modified_at
        }


    @classmethod
    def find_user_by_email(cls, email):
        user = cls.query.filter_by(email=email).first()
        if user:
            return user
        else:
            return None

    @classmethod
    def find_user_by_id(cls,user_id):
        user = cls.query.filter_by(user_id=user_id).first()
        if user:
            return user
        else:
            return None

    def __generate_hash(self, password):
        return bcrypt.generate_password_hash(password, rounds=10).decode("utf-8")

    def check_hash(self, password):
        return bcrypt.check_password_hash(self.senha, password)

