from flask_restful import Resource, reqparse
from src.models.user import User
from  datetime import datetime, timedelta
from flask_jwt_extended import  create_access_token, create_refresh_token, get_current_user


class Auth(Resource):
    def post(self):
        atributos = reqparse.RequestParser()
        atributos.add_argument('email', type=str, required=True, help='the filed login be left blanc')
        atributos.add_argument('password', type=str, required=True, help='the filed password be left blanc')
        json = atributos.parse_args()
        
        user = User.find_by_email(User, json.get("email"))
        if user and user.check_hash(json.get("password")):
            expires_token = timedelta(seconds=3600)
            expires_refresh = timedelta(days=1)
            ret = {
                    'access_token':create_access_token(identity=user.nome, expires_delta=expires_token),
                    'expires_in_token':expires_token.total_seconds(),
                    'refresh_token':create_refresh_token(identity=user.nome),
                    'expires_refresh_token':expires_refresh.total_seconds()
                }
            return ret, 200
        else:
            return {"errors":"the user whit email {} not found".format(json.get("email"))}

class Refresh(Resource):
    def post(self):
        expires_token = timedelta(seconds=3600)
        expires_refresh = timedelta(days=1)
        current_user = get_current_user()
        ret = {
        'access_token':create_access_token(identity=current_user),
        'expires_in_token':expires_token.total_seconds(),
        'refresh_token':create_refresh_token(identity=current_user),
        'expires_refresh_token':expires_refresh.total_seconds()
        }
        return ret, 200
