from flask_restful import Resource, reqparse
from ..models.user import User
from flask_jwt_extended import jwt_required


class Registro(Resource):

    @jwt_required
    def post(self):
        atributos = reqparse.RequestParser()
        atributos.add_argument('email', type=str, required=True, help='the filed login be left blanc')
        atributos.add_argument('password', type=str, required=True, help='the filed password be left blanc')
        atributos.add_argument('nome', type=str, required=True, help='the filed senha be left blanc')
        json = atributos.parse_args()
        new_user = User(**json)
        try:
            new_user.save()
            return {
                'message': 'User {} was created'.format(json['nome'])
                }
        except Exception as erro:
            return {'message': '{}'.format(erro)}, 500

